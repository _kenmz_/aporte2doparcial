﻿using System;

namespace Aporte_2Parcial
{
    //Creamos esta clase conectandola con la interfaz que le proveera
    //las implementaciones especificas de los pasos de construcion
    public class ConcreteBuilder : IBuilder
    {
        private Producto producto = new Producto();

        // Una instancia del constructor se debe declarar sin parametros
 
        public ConcreteBuilder()
        {
            Reset();
        }

        public void Reset()
        {
            producto = new Producto();
        }

        // Todos los paso de produccion trabajan con la misma
        //instancia de producto

        //En los siguiente 3 metodo, describiremos que hara cada parte 
        public void BuildInicio()
        {
            producto.AgregarParte("Inicio");
        }

        public void BuildCuerpo()
        {
            producto.AgregarParte("Cuerpo");
        }

        public void BuildFinal()
        {
            producto.AgregarParte("Final");
        }

        //Este metodo nos permitira obtener el producto final
        public Producto ObtenerProducto()
        {
            Producto result = producto;

            Reset();

            return result;
        }
    }
}