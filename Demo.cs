﻿using System;

namespace Aporte_2Parcial
{
    class Demo
    {
        static void Main(string[] args)
        {
            //Instanciamos un objeto de la clase director
            Director director = new Director();
            
            //Instanciamos un objeto de la clase ConcreteBuilder creando asi nuestro builder
            ConcreteBuilder builder = new ConcreteBuilder();

            //Le asignamos nuestro builder a la variable de la instancia Director
            director.Builder = builder;

            
            Console.WriteLine("Producto base:");
            //Establecemos una base a la variable director 
            director.BuildBase();
            //Imprimimos por consola el resultado base de la lista de productos que tenemos asignados en el builder
            Console.WriteLine(builder.ObtenerProducto().ListaDePartes());
            Console.WriteLine();
            Console.WriteLine("Todas las caracteristicas del producto final:");
            //Agregamos las partes restantes para que el producto este completo
            director.BuildCaracteristicas();

            Console.WriteLine(builder.ObtenerProducto().ListaDePartes());

            Console.WriteLine();
            Console.WriteLine("Partes Agregadas:");
            //Mostramos por consola las partes del builder
            builder.BuildCuerpo();
            builder.BuildFinal();
            Console.Write(builder.ObtenerProducto().ListaDePartes());
        }
    }
}
