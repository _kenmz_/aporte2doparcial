﻿using System;

namespace Aporte_2Parcial
{
    public class Director
    {
        private IBuilder builder;

        public IBuilder Builder
        {
            set { builder = value; }
        }

        // El Director puede construir varias variaciones de productos
        // utilizando los mismos pasos de construcción.
        //Este metodo permitira contruir una base del producto final
        public void BuildBase()
        {
            builder.BuildInicio();
        }

        //En cambio aqui, ya agregaremos las caracteristicas que hagan falta
        public void BuildCaracteristicas()
        {
            builder.BuildInicio();
            builder.BuildCuerpo();
            builder.BuildFinal();
        }
    }
}