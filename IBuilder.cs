﻿using System;

namespace Aporte_2Parcial
{
    public interface IBuilder
    {
        //Aqui en nuestra interfaz builder, creamos los metodos
        //para crear diferentes partes de los objetos Productos
        void BuildInicio();

        void BuildCuerpo();

        void BuildFinal();
    }
}