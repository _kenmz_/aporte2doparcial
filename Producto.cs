﻿using System;
using System.Collections.Generic;

namespace Aporte_2Parcial
{
    public class Producto
    {
        //Declaramos una variable tipo lista de objetos que sera nuestro producto
        private List<object> partes = new List<object>();

        //Este metodo nos permite añadir partes a la lista
        public void AgregarParte(string parte)
        {
            partes.Add(parte);
        }

        //Aqui podemos obtener una cadena con las partes del producto
        public string ListaDePartes()
        {
            //seteamos la variable que devolveremos mas adelante con un mensaje defaulty
            string resultado = "Partes: ";

            //iteramos dentro de la lista agregando un slash "/" para diferenciar entre las partes
            for (int i = 0; i < partes.Count; i++)
            {
                resultado += partes[i] + " -> ";
            }

            //eliminamos los ultimos 3 caracteres de la cadena para que se vea ordenado
            resultado = resultado.Remove(resultado.Length - 3);

            //retornamos la cadena con las partes del producto
            return resultado;
        }
    }
}